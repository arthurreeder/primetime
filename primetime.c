#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <pthread.h>

#define N 1000000  // number of primes to generate
#define T 10  // number of seconds to run the benchmark

clock_t start;  // Declare start as a global variable

// Returns 1 if n is prime, 0 otherwise
int is_prime(int n) {
  if (n <= 1) return 0;
  for (int i = 2; i <= sqrt(n); i++) {
    if (n % i == 0) return 0;
  }
  return 1;
}

// Thread function that performs arithmetic operations using the prime numbers
void *thread_func(void *arg) {
  int *primes = (int *)arg;
  long long result = 0;
  while (1) {
    for (int i = 0; i < N; i++) {
      result += primes[i] * primes[(i + 1) % N];
      result %= 123456789;
    }
    // Check if T seconds have elapsed
    if ((clock() - start) / CLOCKS_PER_SEC >= T) break;
  }
  return (void *)result;
}

int main(int argc, char *argv[]) {
  int num_threads = 1;  // Default to 1 thread
  if (argc >= 2) {
    num_threads = atoi(argv[1]);
  }

  int primes[N];
  int count = 0;

  // Generate an array of N prime numbers using the Sieve of Eratosthenes
  for (int i = 2; count < N; i++) {
    if (is_prime(i)) {
      primes[count] = i;
      count++;
    }
  }

  printf("Running prime test with %d cores. Hang tight!\n", num_threads);

  // Start the clock
  start = clock();

  // Create and run num_threads threads
  pthread_t threads[num_threads];
  for (int i = 0; i < num_threads; i++) {
    pthread_create(&threads[i], NULL, thread_func, (void *)primes);
  }

  // Wait for the threads to finish
  long long result = 0;
  for (int i = 0; i < num_threads; i++) {
    void *thread_result;
    pthread_join(threads[i], &thread_result);
    result += (long long)thread_result;
  }

  // Calculate the score
  double elapsed_time = (double)(clock() - start) / CLOCKS_PER_SEC;
  double score = (double)N * num_threads / elapsed_time;

  // Print the result and the score
  printf("Result: %lld\n", result);
  printf("Score: %.2f\n", score);

  return 0;
}
